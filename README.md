<div align="center">
  <a href=""><img width="180" height="180" src="https://gitlab.com/radcolor/TweaksKM/-/raw/master/app/src/main/res/drawable-xxxhdpi/linux.png"></a>
</div>

# TweaksKM: A Linux kernel manager app for Android™ devices

<a href="https://www.android.com"><img src="https://forthebadge.com/images/badges/built-for-android.svg"></a> <a href="https://www.gitlab.com/radcolor"><img src="https://forthebadge.com/images/badges/built-with-love.svg"/></a>

**TweaksKM** is a simple [Linux](https://www.kernel.org) kernel manager app for Android™ devices running on Android L or newer.
Designed beautifully with Material design guidelines.

**Note •** The contributors are requested to read CONTRIBUTING.md and all required documentation, and send pull requests over dev/staging branch!

## Setup and Requirements

- Android SDK and platform tools
- Minimum SDK 21 or newer
- AndroidX and libraries
- Java Development Kit version 8

### Build instructions

```
$ git clone https://github.com/theradcolor/TweaksKM.git
$ cd TweaksKM
$ bash gradlew assembleDebug
```

## Working/Principle

App needs the Android's root permission to attain [privileged control](https://en.wikipedia.org/wiki/Privilege_escalation) (known as [root access](https://en.wikipedia.org/wiki/Superuser)) over various Android subsystems. As Android uses the Linux kernel, rooting an Android device gives similar access to administrative (superuser) permissions as on Linux or any other Unix-like operating system.

## Running screenshots

<div align="center">
<a href=""><img src="https://gitlab.com/radcolor/TweaksKM/-/raw/master/assets/app_ss.png"/></a>
</div>

## Reporting bug or feature request

You can easily report a bug or request a feature by opening a [pull request](https://github.com/radcolor/TweaksKM/compare) or [opening an issue](https://github.com/radcolor/TweaksKM/issues/new/choose)

**How to report a bug/issue**

- Make sure of taking a logs in detail
- Make sure no other similar bugs already reported

**How to request a feature**

- A detail description of feature
- Paths to sysFS interface, How to apply and use of it.

## Opensource licenses

RootUtils/Tools from Kernel Adiutor by [Wille Ye](https://github.com/Grarak) - [GPLv3](https://www.gnu.org/licenses/gpl-3.0)

libsu by [John Wu](https://github.com/topjohnwu) - [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0)

hellochart-android by [Leszek Wach](https://github.com/lecho) - [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0)

## Contributing

Read [Contributing.md](https://gitlab.com/radcolor/TweaksKM/blob/master/CONTRIBUTING.md) to get the app running locally and various ways to help us on development.

## License

<a href="https://gitlab.com/radcolor/TweaksKM/blob/master/LICENSE"><img src="https://img.shields.io/badge/License-GPL--v3.0-green?style=for-the-badge"></a>

TweaksKM is licensed under the under version 3 of the [GNU GPL License](https://github.com/theradcolor/TweaksKM/blob/master/LICENSE).

The GNU General Public License is a free, copyleft license for software and other kinds of works.
When we speak of free software, we are referring to freedom, not price. Our General Public Licenses are designed to make sure that you have the freedom to distribute copies of free software (and charge for them if you wish), that you receive source code or can get it if you want it, that you can change the software or use pieces of it in new free programs, and that you know you can do these things.

Copyright (c) 2020-22 Shashank. All rights reserved.

<div align="center">Made with ❤ by <a href="https://radcolor.dev">Shashank</a></div>
